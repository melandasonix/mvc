<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index' );
Route::get('/register', 'AuthController@index' );
// Route::post('/kirim', 'AuthController@kirim' );
Route::post('/welcome', 'AuthController@kirim' );

Route::get('/welcome', 'AuthController@create' );



// Route::get('/register', function () {
//     return view('Ini halaman register');
// });
// Route::get('/home', function () {
//     return view('welcome1');
// });
